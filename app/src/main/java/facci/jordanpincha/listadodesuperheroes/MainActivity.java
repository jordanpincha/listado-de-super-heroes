package facci.jordanpincha.listadodesuperheroes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycle_view);

        ArrayList<String> tareas = new ArrayList<>();

        for (int item = 0; item<=0; item++){
            tareas.add("AVENGER");
            tareas.add("THOR");
            tareas.add("CAPITAN AMERICA");
            tareas.add("HULK");
            tareas.add("CAPITANA MARVEL");
            tareas.add("IROMAM");
            tareas.add("PANTERA NEGRA");
            tareas.add("ANTMAN");

        }

        TareasRecyclerViewAdapter adapter = new TareasRecyclerViewAdapter(this, tareas);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
